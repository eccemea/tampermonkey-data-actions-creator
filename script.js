// ==UserScript==
// @name         Developer Tools - Create Data Action
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Creates data actions from PureCloud developer tools portal after a query is successful
// @author       Genesys EMEA Cloud Competence Center
// @match        https://developer.mypurecloud.com/
// @match        https://developer.mypurecloud.ie/
// @match        https://developer.mypurecloud.de/
// @match        https://developer.mypurecloud.jp/
// @match        https://developer.mypurecloud.com.au/
// @include      https://developer.mypurecloud.com/*
// @include      https://developer.mypurecloud.ie/*
// @include      https://developer.mypurecloud.de/*
// @include      https://developer.mypurecloud.jp/*
// @include      https://developer.mypurecloud.com.au/*
// @require      https://sdk-cdn.mypurecloud.com/javascript/52.1.1/purecloud-platform-client-v2.min.js
// @grant        GM_xmlhttpRequest
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_addStyle
// @grant        GM_getResourceText
// @grant        GM_getResourceURL
// @run-at       document-idle
// ==/UserScript==

// Wait until div containing the buttons is ready
var timerId = setInterval(gotButtons, 100);
var platformClient;

var observer = new MutationObserver(function (mutations) {
  try {
    let responseCode = document.querySelector(".col-md-12 > div").textContent.trim();
    if (responseCode == "200 OK" || responseCode == "202 Accepted" || responseCode == "201 Created") {
      $('#button-create-data-action')[0].disabled = false;
    }
  }
  catch (error) {
  }
});
observer.observe(document.querySelector('body'), {
  subtree: true,
  childList: true,
  attributes: false,
  characterData: false,
  attributeOldValue: false,
  characterDataOldValue: false
});

function gotButtons() {
  const div = document.querySelector('#collapseRequestHeading > div > div.container-fluid > div > div');
  console.log("div:", div);
  if (!div) {
    return;
  }

  clearInterval(timerId);

  getIntegrations();

  var baseUrl = document.querySelector(".operation-uri").textContent;
  var baseUrlWithQueryParameters = document.querySelector("#requestParamsHeader > label").textContent;
  var method = document.querySelector("#requestParamsHeader > div > label").textContent.toUpperCase();
  var body = document.querySelector(".ace_content") ? document.querySelector(".ace_content").textContent : "";
  console.log("Body:", body);
  //     if (body.length > 0) {
  //         prettyPrint("body");
  //     }

  $("#exampleModal").remove();
  $("body").append(`
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">New Data Action</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      </div>
      <div class="modal-body">
        <form id="createDataActionForm">
          <div class="form-group">
            <label for="integrations" class="col-form-label">Which integration will use this action?</label>
            <select class="form-control" id="integrations"></select>
          </div>
          <div class="form-group">
            <label for="name" class="col-form-label">Action Name</label>
            <input type="text" class="form-control" id="name">
          </div>
         <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="secure" value="secure">
            <label class="form-check-label" for="secure">Secure</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" type="checkbox" id="publish" value="publish">
            <label class="form-check-label" for="publish">Publish Immediately</label>
          </div>
          <div id="debug" style="display:none">
            <div class="form-group">
              <label class="col-form-label">Method</label>
              <label class="form-control" id="method">${method}</label>
            </div>
            <div class="form-group">
              <label class="col-form-label">Base URL</label>
              <label class="form-control" id="baseUrl">${baseUrl}</label>
            </div>
            <div class="form-group">
              <label class="col-form-label">Base URL With Query Parameters</label>
              <label class="form-control" id="baseUrlWithQueryParameters">${baseUrlWithQueryParameters}</label>
            </div>
           <div class="form-group">
              <label class="col-form-label">Body</label>
              <textarea id="body" readonly class="form-control">${body}</textarea>
            </div>
            <div class="form-group">
              <label class="col-form-label">Output</label>
              <pre><code id="output"></code></pre>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" id="create" class="btn btn-primary">Create</button>
      </div>
    </div>
  </div>
</div>
    `);

  // Add "Create Data Action" button
  var button = document.createElement("button");
  button.innerHTML = "Create Data Action";
  button.className = "btn btn-primary";
  button.setAttribute("disabled", "true");
  button.setAttribute("id", "button-create-data-action");
  button.setAttribute("data-toggle", "modal");
  button.setAttribute("data-target", "#exampleModal");
  div.appendChild(button);

  var output;

  // Populate Output when clicked on
  button.addEventListener("click", function () {
    // Get editor id
    var editorControl = $("#collapseResponse [id^=ember]")[0].id;
    console.log("Editor Control:", editorControl);
    var editor = ace.edit(editorControl);
    output = editor.getValue();
    $("#output").html(output);
  });

  function checkboxOnClick(event) {
    var checkbox = event.target;
    if (checkbox.checked) {
      //Checkbox has been checked
      console.log("Checked:", checkbox);
    } else {
      //Checkbox has been unchecked
      console.log("Unchecked:", checkbox);
    }
  }

  $("#create").on("click", () => {
    // Build output variables
    /*
    . name (object name (e.g. “name”)
           . path ($.id or $.entities[*].agent.name)
                . type (array, string, int)
    */
    var outputParameters = buildOutputVariables(output);

    var input = {
      name: $("#exampleModal #name").val(),
      integrationId: $("#exampleModal #integrations").val(),
      apiUrl: $("#exampleModal #baseUrl").text(),
      apiRequest: $("#exampleModal #baseUrlWithQueryParameters").text(),
      method: $("#exampleModal #method").text(),
      body: $("#exampleModal #body").text() == "" ? "" : JSON.parse($("#exampleModal #body").text()),
      outputParameters: outputParameters,
      secure: $("#secure").is(":checked"),
      publish: $("#publish").is(":checked")
    }

    console.log("Send input to createDataAction:", input);
    var dataActionId = createDataAction(input).then((dataActionId) => {
      console.log("Data Action Id:", dataActionId);
      window.open(
        `https://apps.mypurecloud.ie/directory/#/admin/integrations/actions/${dataActionId}/setup/test`,
        '_blank'
      );
      jQuery.noConflict();
      $("#exampleModal").modal("toggle");
    });

  });
}

function buildOutputVariables(output) {
  if (!output) return [];
  var outputVariables = [], rootIsArray = false;
  var outputJSON = JSON.parse(output);
  console.log("Output JSON:", outputJSON);

  // BEGIN FIX CODE ------
  var res = [];
  var isArrayObj = false;
  if (Array.isArray(outputJSON)) {
    console.log('main response is array, get only first item');
    outputJSON = outputJSON[0];
    isArrayObj = true;
  }

  if (outputJSON.hasOwnProperty("entities")) {
    console.log('get only first array element');
    let tmpArray = outputJSON.entities[0];
    outputJSON.entities = [];
    outputJSON.entities.push(tmpArray);
  }


  let temp = flatten(outputJSON)
  console.log(temp);

  for (var elem in temp) {
    var pathElement = elem.replace(/(.[0-99].)/g, '[*].'); // replace .0. to [*]. this is my Path
    pathElement = pathElement.replace(/(.[0-99])/g, '[*]'); // replace last element (if array)

    console.log(pathElement);
    pathElement = isArrayObj ? '$[*].' + pathElement : '$.' + pathElement;
    let newName = elem.replace(/(entities.0.)/g, ''); // TODO
    newName = newName.replace(/([.])/g, '_');

    let myType = pathElement.indexOf("[*]") != -1 ? "array" : typeof temp[elem] == "object" ? "array" : typeof temp[elem];

    if (typeof temp[elem] != "object") {
      let newEntry = {
        name: newName,
        path: pathElement,
        type: myType
      }
      res.push(newEntry);
    }
  }


  return res;

  // END FIXED CODE


  if (outputJSON.length > 0) {
    outputJSON = outputJSON[0];
    rootIsArray = true;
  }

  // Recursively go though all properties
  var res = [];
  (function recurse(obj, current, parentPath) {
    for (var key in obj) {
      var value = obj[key];
      var newKey = (current ? current + "." + key : key); // joined key with dot
      if (value && Array.isArray(value)) {
        console.log("Got an array");
        console.log("value[0]:", value[0]); //TODO IS THIS A STRING IN AN ARRAY?
        console.log("NewKey:", newKey);
        console.log("Current:", current);
        console.log("Key:", key);

        if (typeof value[0] == "string") {
          let path = `${parentPath ? parentPath + "." + key + "[*]" : "$." + newKey + "[*]"}`;
          console.log("Path:", path);
          let newEntry = {
            name: newKey.replace(/\./g, '_'),
            path: path,
            type: type
          }
          res.push(newEntry);
        } else {
          let path = `${parentPath ? parentPath + "." + current : "$." + newKey + "[*]"}`;
          console.log("Path:", path);
          recurse(value[0], newKey, path); // it's an array, only use the first entry to find out which fields are in this array
        }
      } else if (value && typeof value === "object") {
        console.log("Got an object");
        var newParentPath = "$." + (parentPath || "") + newKey.substring(newKey.lastIndexOf("."), newKey.length);
        recurse(value, newKey, newParentPath); // it's a nested object, so do it again and specify the correct parent path
      } else {
        console.log("Got a property"); // it's not an object, so set the property
        var type, path;

        if (parentPath) {
          path = parentPath + "." + key;
        } else {
          path = rootIsArray ? "$[*]." + newKey : "$." + newKey;
        }

        if (parentPath) {
          type = parentPath.indexOf("[*]") != -1 ? "array" : typeof value;
        } else {
          type = rootIsArray ? "array" : typeof value;
        }

        let newEntry = {
          name: newKey.replace(/\./g, '_'),
          path: path,
          type: type
        }
        console.log("New Entry:", newEntry);
        res.push(newEntry);
      }
    }
  })(outputJSON);
  console.log("Result:", res);
  //showOutputVariables(res);
  return res;
}

function showOutputVariables(variables) {
  $.each(variables, (index, variable) => {
    var div = $("<div/>", { class: "form-group form-check" }).appendTo("#createDataActionForm");
    $('<input />', { class: "form-check-input", type: "checkbox", id: variable.name, value: variable.name }).appendTo(div);
    $('<label />', { class: "form-check-label", for: variable.name, text: variable.name, style: "margin-left:5px" }).appendTo(div);
  });
}

function prettyPrint(textAreaElementId) {
  var ugly = document.getElementById(textAreaElementId).value;
  var obj = JSON.parse(ugly);
  var pretty = JSON.stringify(obj, undefined, 4);
  document.getElementById(textAreaElementId).value = pretty;
}

function getIntegrations() {
  platformClient = require('platformClient');
  const client = platformClient.ApiClient.instance;
  client.setEnvironment("mypurecloud.ie");
  client.loginImplicitGrant("4262b72e-669e-4f4f-9f6c-b7223cf8895d", "http://localhost:8080/index.html")
    .then((data) => {
      console.log("Logged In:", data);
      // Do authenticated things
      return new Promise(function (resolve, reject) {

        try {
          let apiInstance = new platformClient.IntegrationsApi();

          let opts = {
            'pageSize': 99,
            'pageNumber': 1
          };

          apiInstance.getIntegrations(opts)
            .then((data) => {
              //console.log(`getIntegrations success! data: ${JSON.stringify(data, null, 2)}`);

              let response = [];

              for (var obj in data.entities) {
                if (data.entities[obj].hasOwnProperty("integrationType") && data.entities[obj].integrationType.id == 'purecloud-data-actions' && data.entities[obj].intendedState == 'ENABLED') {
                  response.push({
                    id: data.entities[obj].id,
                    name: data.entities[obj].name
                  })
                }
              }
              console.log(response);
              $.each(response, (index, integration) => {
                $("#integrations").append($("<option/>", { text: integration.name, value: integration.id }));
              });
              resolve(response);
            })
            .catch((err) => {
              console.log('There was a failure calling getIntegrations');
              console.error(err);
              reject()
            });
        } catch (error) {
          console.error(error);
          reject();
        }
      });
    })
    .catch((err) => {
      // Handle failure response
      console.error(err);
    });
  //     var authData = JSON.parse(localStorage.getItem("purecloud_dev_tools_auth_auth_data"));
  //     var accessToken = authData.accessToken;
  //     console.log("Access Token:", accessToken);


}

function isBuffer(obj) {
  return obj != null && obj.constructor != null &&
    typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj)
}

function flatten(target, opts) {
  opts = opts || {}

  var delimiter = opts.delimiter || '.'
  var maxDepth = opts.maxDepth
  var output = {}

  function step(object, prev, currentDepth) {
    currentDepth = currentDepth || 1
    Object.keys(object).forEach(function (key) {
      var value = object[key]
      var isarray = opts.safe && Array.isArray(value)
      var type = Object.prototype.toString.call(value)
      var isbuffer = isBuffer(value)
      var isobject = (
        type === '[object Object]' ||
        type === '[object Array]'
      )

      var newKey = prev
        ? prev + delimiter + key
        : key

      if (!isarray && !isbuffer && isobject && Object.keys(value).length &&
        (!opts.maxDepth || currentDepth < maxDepth)) {
        return step(value, newKey, currentDepth + 1)
      }

      output[newKey] = value
    })
  }

  step(target)

  return output
}

function unflatten(target, opts) {
  opts = opts || {}

  var delimiter = opts.delimiter || '.'
  var overwrite = opts.overwrite || false
  var result = {}

  var isbuffer = isBuffer(target)
  if (isbuffer || Object.prototype.toString.call(target) !== '[object Object]') {
    return target
  }

  // safely ensure that the key is
  // an integer.
  function getkey(key) {
    var parsedKey = Number(key)

    return (
      isNaN(parsedKey) ||
      key.indexOf('.') !== -1 ||
      opts.object
    ) ? key
      : parsedKey
  }

  var sortedKeys = Object.keys(target).sort(function (keyA, keyB) {
    return keyA.length - keyB.length
  })

  sortedKeys.forEach(function (key) {
    var split = key.split(delimiter)
    var key1 = getkey(split.shift())
    var key2 = getkey(split[0])
    var recipient = result

    while (key2 !== undefined) {
      var type = Object.prototype.toString.call(recipient[key1])
      var isobject = (
        type === '[object Object]' ||
        type === '[object Array]'
      )

      // do not write over falsey, non-undefined values if overwrite is false
      if (!overwrite && !isobject && typeof recipient[key1] !== 'undefined') {
        return
      }

      if ((overwrite && !isobject) || (!overwrite && recipient[key1] == null)) {
        recipient[key1] = (
          typeof key2 === 'number' &&
            !opts.object ? [] : {}
        )
      }

      recipient = recipient[key1]
      if (split.length > 0) {
        key1 = getkey(split.shift())
        key2 = getkey(split[0])
      }
    }

    // unflatten again for 'messy objects'
    recipient[key1] = unflatten(target[key], opts)
  })

  return result
}

function createDataAction(_input) {
  return new Promise(function (resolve, reject) {
    try {
      let baseUrl = 'https://localhost';
      let parameters = _input.apiUrl.match(/\{(.*?)\}/g)
      let splitBase = _input.apiUrl.split('/')
      var requestUrl = ''
      var requestTemplate = '${input.rawRequest}'

      if (parameters) {
        for (var i = 0; i < splitBase.length; i++) {
          if (splitBase[i] == '') continue
          let indexInputParam = parameters.indexOf(splitBase[i]);

          if (indexInputParam >= 0) {
            requestUrl = requestUrl + '/' + `\${input.${parameters[indexInputParam].replace(/[{}]/g, '')}}`
          } else {
            requestUrl = requestUrl + '/' + splitBase[i]
          }
        }
      } else {
        requestUrl = _input.apiUrl;
        parameters = [];
      }

      // Check if there are params in URL to inlclude
      var url_string = baseUrl + _input.apiRequest;
      var url = new URL(url_string);

      var paramConnector = '?';

      let searchItems = url.searchParams.entries();
      var aItem = searchItems.next();
      while (!aItem.done) {
        parameters.push(aItem.value[0]);
        requestUrl = `${requestUrl}${paramConnector}${aItem.value[0]}=\${input.${aItem.value[0]}}`
        aItem = searchItems.next();
        paramConnector = '&';
      }

      // find input parameters from POST Body (if exists)
      if (_input.body) {
        let flat = flatten(_input.body);
        for (var key in flat) {
          let parameterName = key.replace(/[.]/g, '_');
          parameters.push(parameterName);
          flat[key] = `\${input.${parameterName}}`
        }
        _input.body = unflatten(flat);
        requestTemplate = JSON.stringify(_input.body);

      }

      console.log(`inputParameters ${parameters}`); // input parameters for Contract
      console.log(`requestUrl ${requestUrl}`); // requestUrl for DataAction

      // Build inputCotract
      var inputCotract = {};
      for (var i = 0; i < parameters.length; i++) {
        let name = parameters[i].replace(/[{}]/g, '');
        inputCotract[name] = {
          type: 'string',
          title: name
        }
      }

      // Build outputContract
      var outputContract = {};
      var outputTranslationMap = {};
      var outputSuccessTemplate = "{ ";

      for (var j = 0; j < _input.outputParameters.length; j++) {
        let item = _input.outputParameters[j];
        outputContract[item.name] = {
          type: item.type.toLowerCase(),
          title: item.name.toLowerCase()
        };
        outputTranslationMap[item.name.toLowerCase()] = item.path;
        outputSuccessTemplate = outputSuccessTemplate + " \"" + item.name.toLowerCase() + "\": ${" + item.name.toLowerCase() + "} ,";
      }

      // remove last character & close bracket
      outputSuccessTemplate = outputSuccessTemplate.substring(0, outputSuccessTemplate.length - 1) + "}";
      console.log(`outputSuccessTemplate: ${outputSuccessTemplate}`);


      // response successTemplate

      // Create DataActionBody
      let dataActionBody = {
        category: "PureCloud Data Actions",
        name: _input.name,
        integrationId: _input.integrationId,
        contract: {
          input: {
            inputSchema: {
              $schema: "http://json-schema.org/draft-04/schema#",
              title: "Input parameters",
              description: "Input parameters",
              type: "object",
              properties: inputCotract
            }
          },
          output: {
            successSchema: {
              $schema: "http://json-schema.org/draft-04/schema#",
              title: "Response",
              description: "Response",
              type: "object",
              properties: outputContract
            }
          }
        },
        config: {
          request: {
            requestUrlTemplate: requestUrl,
            requestTemplate: requestTemplate,
            requestType: _input.method,
            headers: {
              UserAgent: "PureCloudIntegrations/1.0",
              'Content-Type': "application/json"
            }
          },
          response: {
            translationMap: outputTranslationMap,
            translationMapDefaults: {},
            successTemplate: outputSuccessTemplate
          }
        },
        secure: _input.secure
      }
      console.log(JSON.stringify(dataActionBody));

      // Create DataAction

      let apiInstance = new platformClient.IntegrationsApi();

      if (_input.publish) {
        apiInstance.postIntegrationsActions(dataActionBody)
          .then((data) => {
            console.log(`postIntegrationsActionsDrafts success! data: ${JSON.stringify(data, null, 2)}`);
            console.log(`dataAtionId: ${data.id}`);
            resolve(data.id)
            // https://apps.mypurecloud.ie/directory/#/admin/integrations/actions/custom_-_da879bdb-25cc-4dd5-ae4c-a64a3aeabd1e/setup/test
          })
          .catch((err) => {
            console.log('There was a failure calling postIntegrationsActionsDrafts');
            console.error(err);
            reject()
          });
      } else {
        apiInstance.postIntegrationsActionsDrafts(dataActionBody)
          .then((data) => {
            console.log(`postIntegrationsActionsDrafts success! data: ${JSON.stringify(data, null, 2)}`);
            console.log(`dataAtionId: ${data.id}`);
            resolve(data.id)
            // https://apps.mypurecloud.ie/directory/#/admin/integrations/actions/custom_-_da879bdb-25cc-4dd5-ae4c-a64a3aeabd1e/setup/test
          })
          .catch((err) => {
            console.log('There was a failure calling postIntegrationsActionsDrafts');
            console.error(err);
            reject()
          });
      }
    } catch (error) {
      console.error(error);
      reject();
    }
  });
}
