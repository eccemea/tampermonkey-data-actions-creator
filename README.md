# TamperMonkey data actions creator

This repository holds the source code for the Tampermonkey script that creates a data action once a query has successfully completed on PureCloud Developer Tools portal.

* This is provided free of charge but is not actively maintained nor supported.
* This is **NOT** an official Genesys script.
* **DO NOT** open a ticket with the Genesys Customer Care about this script.

## Requirements

* Google Chrome
* or Mozilla Firefox (not tested though but should work)

## Installation

* Install [TamperMonkey](https://www.tampermonkey.net/)
* Open the TamperMonkey Dashboard by clicking on the TamperMonkey icon
* Create a new script by clicking on the `+` sign next to `Installed userscripts` on the top-right
* Delete the code that is auto-generated and replace it with the contents of the `script.js` file from this repository

## Instructions

* Go to the PureCloud developer tools portal (https://developer.mypurecloud.com (or .ie, .de, .com.au, .jp depending on your region)) and open the `API Explorer`
* You should see an extra button on the right side of the `Share Request` button called `Create Data Action` but at this point, it's disabled and that's expected
* Run a successful query (e.g. do a GET /users/me)
* The `Create Data Action` button should now be enabled. Click on it and follow the steps to create the data action based on your current query

## Limitations

* If you have multiple tabs inside the developer tools API explorer open, the button will only show on the first tab
* There is an issue when an array of booleans is in the response that could cause the data action to not work properly. As there is no easy way to fix this, you might have to fix the data action yourself
* This is a prototype created during an internal hackathon so there could be bugs